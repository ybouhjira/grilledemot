#ifndef DICTIONNAIRE_H
#define DICTIONNAIRE_H

#include <vector>
#include <cctype>
#include <string>

class Dictionnaire{
// TYPES
private:
    class Noeud{
    public:
        Noeud(char lettre);
        char lettre(){
            return m_lettre;
        }

        void setLettre(char lettre){
            m_lettre = lettre ;
        }

        std::vector<Noeud*>& enfants(){
            return m_enfants;
        }

        Noeud* AjouterEnfant(char lettre){
            int c=m_enfants.size();
            for(int i=0; i<c; i++)
                if(tolower(lettre) == tolower(m_enfants[i]->lettre()))
                    return m_enfants[i];
            Noeud* n = new Noeud(lettre) ;
            m_enfants.push_back(n);
            return n;
        }

    private:
        char m_lettre;
        std::vector< Noeud* > m_enfants;
    };

public:
    enum Reponse{NON,COMMENCE,EGALE};
    
// METHODES
public:
    Dictionnaire(char* const nomFichier) ;
    Reponse motExist(std::string const& mot) const;

private:
    Noeud* ajouterLettre(char);
    Noeud* noeud(char lettre);
    
// CHAMPS
private:
     std::vector<Noeud* > m_lettres;
};
#endif
