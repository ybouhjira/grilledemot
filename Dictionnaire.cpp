#include "Dictionnaire.h"
#include <fstream>
#include <string>
#include <iostream>
using namespace std;

Dictionnaire::Dictionnaire(char* nomFichier){
    fstream fichier(nomFichier);
    if(!fichier)
        cout << "Impossible d'ouvrir le fichier" << endl;

    vector<string> dictList;
    char mot[30];
    while(fichier.getline(mot,30)){
        dictList.push_back(string(mot));
    }


    for(int i=0, size=dictList.size(); i<size; i++){
        string mot_cpp(dictList[i]);
        Noeud *parent = ajouterLettre(tolower(mot_cpp[0]));
        int j=1;
        for(int c=mot_cpp.size()-1; j<c; j++)
            parent = parent->AjouterEnfant(tolower(mot_cpp[j]));
        parent = parent->AjouterEnfant(toupper(mot_cpp[j]));
    }
}

Dictionnaire::Noeud::Noeud(char lettre){
    m_lettre = lettre ;
}

Dictionnaire::Noeud* Dictionnaire::ajouterLettre(char lettre){
    int c= m_lettres.size();
    for(int i=0; i<c; i++)
        if(tolower(lettre) == tolower(m_lettres[i]->lettre()) )
            return m_lettres[i];
    Noeud* n = new Noeud(lettre);
    m_lettres.push_back(n);
    return n;
}

Dictionnaire::Noeud* Dictionnaire::noeud(char lettre){
    for(int i=0, c=m_lettres.size(); i<c; i++)
        if(tolower(lettre) == tolower(m_lettres[i]->lettre()))
            return m_lettres[i];
    return 0;
}

Dictionnaire::Reponse Dictionnaire::motExist(string const &m) const{
    Noeud* act = 0;
    // premi�re lettre
    bool trouve = false ;
    for(int i=0,c=m_lettres.size(); i<c; i++){
        if(tolower(m_lettres[i]->lettre()) == tolower(m[0])){
            trouve = true ;
            act = m_lettres[i] ;
            break;
        }
    }
    if(!trouve)
        return NON ;

    // lettre du milieu
    for(int i=1,c=m.length()-1; i<c; i++){
        bool contient = false ;
        for(int j=0,s=act->enfants().size(); j<s; j++){
            if(tolower(act->enfants()[j]->lettre()) == tolower(m[i])){
                contient = true;
                act = act->enfants()[j] ;
                break;
            }
        }
        if(!contient)
            return NON;
    }

    // derni�re lettre
    for(int i=0, c=act->enfants().size(); i<c; i++){
        if(act->enfants()[i]->lettre() == toupper(m[m.size()-1])){
            return EGALE;
        }else if(toupper(act->enfants()[i]->lettre()) == toupper(m[m.size()-1])){
            return COMMENCE;
        }
    }
    return NON;

}
