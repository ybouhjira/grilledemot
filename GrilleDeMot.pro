#-------------------------------------------------
#
# Project created by QtCreator 2012-10-25T19:35:24
#
#-------------------------------------------------

QT       -= gui core

TARGET = GrilleDeMot
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    Dictionnaire.cpp

HEADERS += Dictionnaire.h

OTHER_FILES += \
    grille.txt \
    francais.dic
