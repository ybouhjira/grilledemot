#define FICHIER_GRILLE "grille.txt"
#define FICHIER_DICTIONNAIRE "francais.dic"
#define TAILLE_GRILLE 18

#include <exception>
#include <iostream>
#include <fstream>
#include "Dictionnaire.h"
#include <stdlib.h>
#include <string>
#include <iomanip>

using namespace std;

char** lireGrille() ;

void afficherGrille(char** grille){
    for(int i=0; i<TAILLE_GRILLE; i++){
        for(int j=0; j<TAILLE_GRILLE; j++){
            cout << grille[i][j] << ' ';
        }
        cout << endl ;
    }
}

void rechercher(char** grille, Dictionnaire const& dict,int i, int j, int tx,
 int ty)
{
    string motH = string("") + grille[i][j] ;
    Dictionnaire::Reponse rep = dict.motExist(motH) ;
    
    for(int x=tx, y=ty;
        rep != Dictionnaire::NON && 0<i+x && i+x<TAILLE_GRILLE && j+y<TAILLE_GRILLE && j+y >0
        ; x+=tx, y+=ty )
    {
        if(rep == Dictionnaire::EGALE){
            cout << "| " << setw(16) << setfill(' ') << motH ;
            cout << " | " << setw(5) << setfill(' ') << i ;
            cout << " | " << setw(7) << setfill(' ') << j ;
            cout << " | "   ;
            if(tx != 0 && ty == 0)
                cout << setw(10) << setfill(' ') << "Horizontal" ;
            else if(tx == 0 && ty != 0)
                cout  << setw(10) << setfill(' ') <<"Vertical" ;
            else
                cout << setw(10) << setfill(' ') << "Diagonal" ;
            cout << " | "<< endl ;
        }
        motH += grille[i+x][j+y] ;
        rep = dict.motExist(motH) ;
    }
}

int main( /*int argc, char *argv[]*/ )
{
    Dictionnaire dict(FICHIER_DICTIONNAIRE);
    char** grille = lireGrille();
    if(grille == 0){
        cout << "Impossible de lire le fichier de la grille" << endl ;
        return 1;
    }

    // Parcourir la grille
    cout << "+------------------+-------+---------+------------+" << endl;
    cout << "| " << setw(16) << setfill(' ') << "Mot";
    cout << " | ligne | colonne | " << setw(10) << setfill(' ') << "Direction";
    cout << " |" << endl;
    cout << "+------------------+-------+---------+------------+" << endl;
    
    for(int i=0; i<TAILLE_GRILLE; i++){
        for(int j=0; j<TAILLE_GRILLE; j++){
            rechercher(grille,dict,i,j,1,0);
            rechercher(grille,dict,i,j,0,1);
            rechercher(grille,dict,i,j,-1,0);
            rechercher(grille,dict,i,j,0,-1);
            rechercher(grille,dict,i,j,1,1);
            rechercher(grille,dict,i,j,1,-1);
            rechercher(grille,dict,i,j,-1,-1);
            rechercher(grille,dict,i,j,-1,1);
        }
    }
    
    cout << "+------------------+-------+---------+------------+" << endl;
    return 0;
}

char** lireGrille(){
    // ouverture du fichier
    ifstream fichierGrille(FICHIER_GRILLE);
    if(!fichierGrille)
        return 0;
    //lecture
    char** grille;
    grille = new char*[TAILLE_GRILLE];
    for(int i=0; i<TAILLE_GRILLE; i++)
        grille[i] = new char[TAILLE_GRILLE];
    for(int i=0; i<TAILLE_GRILLE; i++ ){
        for(int j=0; j<TAILLE_GRILLE; j++ ){
            grille[i][j] = fichierGrille.get(); // lettre
            fichierGrille.get(); // \n
        }
    }
    return grille;
}
